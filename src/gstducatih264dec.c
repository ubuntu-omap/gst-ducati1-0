/*
 * GStreamer
 * Copyright (c) 2010, Texas Instruments Incorporated
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * SECTION:element-ducatih264dec
 *
 * FIXME:Describe ducatih264dec here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! ducatih264dec ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h>
#include "gstducatih264dec.h"


#define GST_BUFFER_FLAG_B_FRAME (GST_BUFFER_FLAG_LAST << 0)
#define PADX  32
#define PADY  24

/* This structure is not public, this should be replaced by
   sizeof(sErrConcealLayerStr) when it is made so. */
#define SIZE_OF_CONCEALMENT_DATA 65536

#define gst_ducati_h264dec_parent_class parent_class
G_DEFINE_TYPE (GstDucatiH264Dec, gst_ducati_h264dec, GST_TYPE_DUCATIVIDDEC);

/* *INDENT-OFF* */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264, "
        "stream-format = byte-stream, "   /* only byte-stream */
        "alignment = au, "          /* only entire frames */
        "width = (int)[ 16, 2048 ], "
        "height = (int)[ 16, 2048 ], "
        "framerate = (fraction)[ 0, max ],"
        "profile = (string){constrained-baseline, baseline, main, extended};"
        "video/x-h264, "
        "stream-format = byte-stream, "   /* only byte-stream */
        "alignment = au, "          /* only entire frames */
        "width = (int)[ 16, 2048 ], "
        "height = (int)[ 16, 2048 ], "
        "framerate = (fraction)[ 0, max ],"
        "profile = (string) {high, high-10-intra, high-10, high-4:2:2-intra, "
        "high-4:2:2, high-4:4:4-intra, high-4:4:4, cavlc-4:4:4-intra}, "
        "level = (string) {1, 1b, 1.1, 1.2, 1.3, 2, 2.1, 2.2, 3, 3.1, 3.2, 4, 4.1, 4.2, 5.1};")
    );

static const struct
{
  const char *level;
  gint kb;
} max_dpb_by_level[] = {
  { "1", 149 },
  { "1b", 149 }, /* That one's not in the spec ?? */
  { "1.1", 338 },
  { "1.2", 891 },
  { "1.3", 891 },
  { "2", 891 },
  { "2.1", 1782 },
  { "2.2", 3038 },
  { "3", 3038 },
  { "3.1", 6750 },
  { "3.2", 7680 },
  { "4", 12288 },
  { "4.1", 12288 },
  { "4.2", 12288 },
  { "5", 41400 },
  { "5.1", 69120 },
};
/* *INDENT-ON* */

/* GstDucatiVidDec vmethod implementations */

static void
gst_ducati_h264dec_update_buffer_size (GstDucatiVidDec * self)
{
  gint w = self->width;
  gint h = self->height;

  /* calculate output buffer parameters: */
  self->padded_width = ALIGN2 (w + (2 * PADX), 7);
  self->padded_height = h + 4 * PADY;
  self->min_buffers = MIN (16, 32768 / ((w / 16) * (h / 16))) + 3;
}

static gboolean
gst_ducati_h264dec_allocate_params (GstDucatiVidDec * self, gint params_sz,
    gint dynparams_sz, gint status_sz, gint inargs_sz, gint outargs_sz)
{
  gboolean ret = GST_DUCATIVIDDEC_CLASS (parent_class)->allocate_params (self,
      sizeof (IH264VDEC_Params), sizeof (IH264VDEC_DynamicParams),
      sizeof (IH264VDEC_Status), sizeof (IH264VDEC_InArgs),
      sizeof (IH264VDEC_OutArgs));

  if (ret) {
    IH264VDEC_Params *params = (IH264VDEC_Params *) self->params;

    params->dpbSizeInFrames = IH264VDEC_DPB_NUMFRAMES_AUTO;
    params->pConstantMemory = 0;
    params->presetLevelIdc = IH264VDEC_LEVEL41;
    params->errConcealmentMode = IH264VDEC_APPLY_CONCEALMENT;
    params->temporalDirModePred = TRUE;

    if (self->codec_debug_info) {
      /* We must allocate a byte per MB, plus the size of some struture which
         is not public. Place this in the first metadata buffer slot, and ask
         for MBINFO metadata for it. */
      GstDucatiH264Dec *h264dec = GST_DUCATIH264DEC (self);
      unsigned mbw = (self->width + 15) / 16;
      unsigned mbh = (self->height + 15) / 16;
      unsigned nmb = mbw * mbh;

      h264dec->bo_mberror =
          omap_bo_new (self->device, nmb + SIZE_OF_CONCEALMENT_DATA,
          OMAP_BO_WC);
      self->outBufs->descs[2].memType = XDM_MEMTYPE_BO;
      self->outBufs->descs[2].buf =
          (XDAS_Int8 *) omap_bo_handle (h264dec->bo_mberror);
      self->outBufs->descs[2].bufSize.bytes = nmb + SIZE_OF_CONCEALMENT_DATA;
      self->params->metadataType[0] = IVIDEO_METADATAPLANE_MBINFO;
    }
  }

  return ret;
}

static gint
gst_ducati_h264dec_handle_error (GstDucatiVidDec * self, gint ret,
    gint extended_error, gint status_extended_error)
{
  GstDucatiH264Dec *h264dec = GST_DUCATIH264DEC (self);
  const unsigned char *mberror, *mbcon;
  unsigned mbw, mbh, nmb;
  uint16_t mbwr, mbhr;
  size_t n, nerr = 0;
  char *line;
  unsigned x, y;

  if (h264dec->bo_mberror) {
    mberror = omap_bo_map (h264dec->bo_mberror);
    mbw = (self->width + 15) / 16;
    mbh = (self->height + 15) / 16;
    nmb = mbw * mbh;
    mbcon = mberror + nmb;
    mbwr = ((const uint16_t *) mbcon)[21];      /* not a public struct */
    mbhr = ((const uint16_t *) mbcon)[22];      /* not a public struct */
    if (nmb != mbwr * mbhr) {
      GST_WARNING_OBJECT (self, "Failed to find MB size - "
          "corruption might have happened");
    } else {
      for (n = 0; n < nmb; ++n) {
        if (mberror[n])
          ++nerr;
      }
      GST_INFO_OBJECT (self, "Frame has %zu MB errors over %zu (%u x %u) MBs",
          nerr, nmb, mbwr, mbhr);
      line = g_malloc (mbw + 1);
      for (y = 0; y < mbh; y++) {
        line[mbw] = 0;
        for (x = 0; x < mbw; x++) {
          line[x] = mberror[x + y * mbw] ? '!' : '.';
        }
        GST_INFO_OBJECT (self, "MB: %4u: %s", y, line);
      }
      g_free (line);
    }
  }

  if (extended_error & 0x00000001) {
    GST_WARNING_OBJECT (self,
        "No valid slice... got to flush and skip to next KeyFrame");
    /* No valid slice. This seems to be bad enough that it's better to flush and
     * skip to the next keyframe.
     */

    if (extended_error == 0x00000201) {
      /* the codec doesn't unlock the input buffer in this case... */
      gst_video_codec_frame_unref ((GstVideoCodecFrame *) self->inArgs->
          inputID);
      self->inArgs->inputID = 0;
    }

    self->needs_flushing = TRUE;
  }

  ret =
      GST_DUCATIVIDDEC_CLASS (parent_class)->handle_error (self, ret,
      extended_error, status_extended_error);

  return ret;
}

static gboolean
gst_ducati_h264dec_query (GstDucatiVidDec * vdec, GstPad * pad,
    GstQuery * query, gboolean * forward)
{
  GstDucatiH264Dec *self = GST_DUCATIH264DEC (vdec);
  gboolean res = TRUE;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_LATENCY:
    {
      gboolean live;
      GstClockTime min, max, latency;

      if (vdec->input_state->info.fps_d == 0) {
        GST_INFO_OBJECT (self, "not ready to report latency");
        res = FALSE;
        break;
      }

      gst_query_parse_latency (query, &live, &min, &max);
      if (vdec->input_state->info.fps_n != 0)
        latency = gst_util_uint64_scale (GST_SECOND,
            vdec->input_state->info.fps_d, vdec->input_state->info.fps_n);
      else
        latency = 0;

      /* FIXME: How do we get that info from the decoder?
       *
       * Take into account the backlog frames for reordering
       * latency *= (vdec->backlog_maxframes + 1);
       */

      if (min == GST_CLOCK_TIME_NONE)
        min = latency;
      else
        min += latency;

      if (max != GST_CLOCK_TIME_NONE)
        max += latency;

      GST_INFO_OBJECT (self,
          "latency %" GST_TIME_FORMAT " ours %" GST_TIME_FORMAT,
          GST_TIME_ARGS (min), GST_TIME_ARGS (latency));
      gst_query_set_latency (query, live, min, max);
      break;
    }
    default:
      break;
  }

  if (res)
    res =
        GST_DUCATIVIDDEC_CLASS (parent_class)->query (vdec, pad, query,
        forward);
  return res;
}

/* GObject vmethod implementations */

static void
gst_ducati_h264dec_finalize (GObject * obj)
{
  GstDucatiH264Dec *self = GST_DUCATIH264DEC (obj);
  if (self->bo_mberror)
    omap_bo_del (self->bo_mberror);
  G_OBJECT_CLASS (parent_class)->finalize (obj);
}


static void
gst_ducati_h264dec_class_init (GstDucatiH264DecClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstDucatiVidDecClass *bclass = GST_DUCATIVIDDEC_CLASS (klass);

  gst_element_class_set_static_metadata (element_class,
      "DucatiH264Dec", "Codec/Decoder/Video",
      "Decodes video in H.264/bytestream format with ducati",
      "Rob Clark <rob@ti.com>");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  bclass->codec_name = "ivahd_h264dec";
  bclass->update_buffer_size =
      GST_DEBUG_FUNCPTR (gst_ducati_h264dec_update_buffer_size);
  bclass->allocate_params =
      GST_DEBUG_FUNCPTR (gst_ducati_h264dec_allocate_params);
  bclass->handle_error = GST_DEBUG_FUNCPTR (gst_ducati_h264dec_handle_error);
  bclass->query = GST_DEBUG_FUNCPTR (gst_ducati_h264dec_query);
  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_ducati_h264dec_finalize);
}

static void
gst_ducati_h264dec_init (GstDucatiH264Dec * self)
{
#ifndef GST_DISABLE_GST_DEBUG
  GstDucatiVidDec *dec = GST_DUCATIVIDDEC (self);

  dec->error_strings[0] = "no error-free slice";
  dec->error_strings[1] = "error parsing SPS";
  dec->error_strings[2] = "error parsing PPS";
  dec->error_strings[3] = "error parsing slice header";
  dec->error_strings[4] = "error parsing MB data";
  dec->error_strings[5] = "unknown SPS";
  dec->error_strings[6] = "unknown PPS";
  dec->error_strings[7] = "invalid parameter";
  dec->error_strings[16] = "unsupported feature";
  dec->error_strings[17] = "SEI buffer overflow";
  dec->error_strings[18] = "stream end";
  dec->error_strings[19] = "no free buffers";
  dec->error_strings[20] = "resolution change";
  dec->error_strings[21] = "unsupported resolution";
  dec->error_strings[22] = "invalid maxNumRefFrames";
  dec->error_strings[23] = "invalid mbox message";
  dec->error_strings[24] = "bad datasync input";
  dec->error_strings[25] = "missing slice";
  dec->error_strings[26] = "bad datasync param";
  dec->error_strings[27] = "bad hw state";
  dec->error_strings[28] = "temporal direct mode";
  dec->error_strings[29] = "display width too small";
  dec->error_strings[30] = "no SPS/PPS header";
  dec->error_strings[31] = "gap in frame num";
#endif
}
