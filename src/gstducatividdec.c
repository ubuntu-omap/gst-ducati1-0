#define USE_DTS_PTS_CODE
/*
 * GStreamer
 * Copyright (c) 2010, Texas Instruments Incorporated
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gstducatipriv.h"
#include "gstducatividdec.h"

#include <sys/drm/gstdrmmeta.h>
#include <sys/dmabuf/gstdmabufmeta.h>

#define gst_ducati_viddec_parent_class parent_class
G_DEFINE_TYPE (GstDucatiVidDec, gst_ducati_viddec, GST_TYPE_VIDEO_DECODER);

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("NV12"))
    );

enum
{
  PROP_0,
  PROP_VERSION,
  PROP_CODEC_DEBUG_INFO
};

/***********************************
 *                                 *
 *         helper functions        *
 *                                 *
 ***********************************/
static void
engine_close (GstDucatiVidDec * self)
{
  if (self->engine) {
    Engine_close (self->engine);
    self->engine = NULL;
  }

  if (self->params) {
    dce_free (self->params);
    self->params = NULL;
  }

  if (self->dynParams) {
    dce_free (self->dynParams);
    self->dynParams = NULL;
  }

  if (self->status) {
    dce_free (self->status);
    self->status = NULL;
  }

  if (self->inBufs) {
    dce_free (self->inBufs);
    self->inBufs = NULL;
  }

  if (self->outBufs) {
    dce_free (self->outBufs);
    self->outBufs = NULL;
  }

  if (self->inArgs) {
    dce_free (self->inArgs);
    self->inArgs = NULL;
  }

  if (self->outArgs) {
    dce_free (self->outArgs);
    self->outArgs = NULL;
  }

  if (self->device) {
    dce_deinit (self->device);
    self->device = NULL;
  }
}

static gboolean
engine_open (GstDucatiVidDec * self)
{
  int ec;
  gboolean ret;

  if (G_UNLIKELY (self->engine)) {
    return TRUE;
  }

  if (self->device == NULL) {
    self->device = dce_init ();
    if (self->device == NULL) {
      GST_ERROR_OBJECT (self, "dce_init() failed");
      return FALSE;
    }
  }

  GST_DEBUG_OBJECT (self, "opening engine");

  self->engine = Engine_open ((String) "ivahd_vidsvr", NULL, &ec);
  if (G_UNLIKELY (!self->engine)) {
    GST_ERROR_OBJECT (self, "could not create engine");
    return FALSE;
  }

  ret = GST_DUCATIVIDDEC_GET_CLASS (self)->allocate_params (self,
      sizeof (IVIDDEC3_Params), sizeof (IVIDDEC3_DynamicParams),
      sizeof (IVIDDEC3_Status), sizeof (IVIDDEC3_InArgs),
      sizeof (IVIDDEC3_OutArgs));

  return ret;
}

static void
codec_delete (GstDucatiVidDec * self)
{
  GST_DEBUG_OBJECT (self, "Deleting codec");

  /* FIXME: Do we need to protect the pool here? */
  if (self->pool) {
    gst_object_unref (self->pool);
    self->pool = NULL;
  }

  if (self->codec) {
    VIDDEC3_delete (self->codec);
    self->codec = NULL;
  }

  if (self->input_bo) {
    omap_bo_del (self->input_bo);
    self->input_bo = NULL;
  }
}

static gboolean
codec_create (GstDucatiVidDec * self)
{
  gint err, n;
  const gchar *codec_name;

  codec_delete (self);

  if (G_UNLIKELY (!self->engine))
    goto no_engine;

  /* these need to be set before VIDDEC3_create */
  self->params->maxWidth = self->width;
  self->params->maxHeight = self->height;

  codec_name = GST_DUCATIVIDDEC_GET_CLASS (self)->codec_name;

  /* create codec: */
  GST_DEBUG_OBJECT (self, "creating codec: %s", codec_name);
  self->codec = VIDDEC3_create (self->engine, (String) codec_name,
      self->params);

  if (!self->codec)
    goto no_codec;

  if (VIDDEC3_control (self->codec, XDM_SETPARAMS, self->dynParams,
          self->status))
    goto setparams_fail;

  self->first_in_buffer = TRUE;

  /* allocate input buffer and initialize inBufs: */
  /* FIXME:  needed size here has nothing to do with width * height */
  self->input_bo = omap_bo_new (self->device,
      2 * self->width * self->height, OMAP_BO_WC);
  self->input = omap_bo_map (self->input_bo);
  self->inBufs->numBufs = 1;
  self->inBufs->descs[0].buf = (XDAS_Int8 *) omap_bo_handle (self->input_bo);

  /* Actual buffers will be set later, as they will be different for every
     frame. We allow derived classes to add their own buffers, however, so
     we initialize the number of outBufs here, counting the number of extra
     buffers required, including "holes" in planes, which may not be filled
     if not assigned. */
  self->outBufs->numBufs = 2;   /* luma and chroma planes, always */
  for (n = 0; n < 3; n++)
    if (self->params->metadataType[n] != IVIDEO_METADATAPLANE_NONE)
      self->outBufs->numBufs = 2 + n + 1;

  return TRUE;

  /* Error cases */
no_engine:
  {
    GST_ERROR_OBJECT (self, "no engine");
    return FALSE;
  }

no_codec:
  {
    GST_ERROR_OBJECT (self, "Failed to create codec %s", codec_name);
    return FALSE;
  }

setparams_fail:
  {
    GST_ERROR_OBJECT (self, "failed XDM_SETPARAMS");
    return FALSE;
  }
}

static XDAS_Int32
codec_prepare_outbuf (GstDucatiVidDec * self, GstVideoCodecFrame * frame)
{
  GstDucatiPriv *priv = NULL;

  priv = gst_buffer_add_ducati_priv (frame->output_buffer,
      self->device, &self->output_state->info);

  if (priv == NULL)
    goto add_priv_failed;

  /* There are at least two buffers. Derived classes may add codec specific
     buffers (eg, debug info) after these two if they want to. */
  self->outBufs->descs[0].memType = XDM_MEMTYPE_BO;
  self->outBufs->descs[0].buf = (XDAS_Int8 *) omap_bo_handle (priv->bo);
  self->outBufs->descs[0].bufSize.bytes = priv->uv_offset;
  self->outBufs->descs[1].memType = XDM_MEMTYPE_BO_OFFSET;
  self->outBufs->descs[1].buf = (XDAS_Int8 *) priv->uv_offset;
  self->outBufs->descs[1].bufSize.bytes = priv->size - priv->uv_offset;

  return (XDAS_Int32) frame;

add_priv_failed:
  {
    GST_WARNING_OBJECT (self, "Can not create private meta, can not use "
        "buffer %p", frame->output_buffer);
    return 0;
  }
}

static inline GstVideoCodecFrame *
codec_add_frame (GstDucatiVidDec * self, XDAS_Int32 id)
{
  GstVideoCodecFrame *frame = (GstVideoCodecFrame *) id;

  if (frame)
    g_hash_table_insert (self->passed_in_bufs, frame,
        gst_video_codec_frame_ref (frame));

  return frame;
}

static inline void
codec_unlock_frame (GstDucatiVidDec * self, XDAS_Int32 id)
{
  GstBuffer *buf = (GstBuffer *) id;

  if (buf) {
    GST_DEBUG_OBJECT (self, "free buffer: %d %p", id, buf);
    g_hash_table_remove (self->passed_in_bufs, buf);
  }
}

static inline void
add_crop_meta (GstDucatiVidDec * self, GstBuffer * buf)
{
  gint crop_width, crop_height;

  if (G_UNLIKELY (self->needs_crop_calculation)) {
    /* send region of interest to sink on first buffer: */
    XDM_Rect *r = &(self->outArgs->displayBufs.bufDesc[0].activeFrameRegion);

    crop_width = r->bottomRight.x - r->topLeft.x;
    crop_height = r->bottomRight.y - r->topLeft.y;

    if (crop_width > self->input_state->info.width)
      crop_width = self->input_state->info.width;
    if (crop_height > self->input_state->info.height)
      crop_height = self->input_state->info.height;

    GST_INFO_OBJECT (self, "active frame region %d, %d, %d, %d, crop %dx%d",
        r->topLeft.x, r->topLeft.y, r->bottomRight.x, r->bottomRight.y,
        crop_width, crop_height);

    self->crop.top = r->topLeft.y;
    self->crop.left = r->topLeft.x, self->crop.width = crop_width;
    self->crop.height = crop_height;

    if ((self->crop.top == 0) && (self->crop.left == 0) &&
        (self->crop.width == self->input_state->info.width) &&
        (self->crop.height == self->input_state->info.height)) {
      self->needs_crop = FALSE;

      GST_DEBUG_OBJECT (self, "Do not need crop");
    } else {
      self->needs_crop = TRUE;

      GST_DEBUG_OBJECT (self, "Needs crop");
    }

    self->needs_crop_calculation = FALSE;
  }

  if (self->needs_crop) {
    GstVideoCropMeta *cropmeta = gst_buffer_add_video_crop_meta (buf);

    if (cropmeta == NULL)
      goto add_meta_failed;

    GST_DEBUG_OBJECT (self, "Adding crop meta x, y, width, height:%d,%d,%d,%d",
        self->crop.left, self->crop.top, self->crop.width, self->crop.height);
    cropmeta->x = self->crop.left;
    cropmeta->y = self->crop.top;
    cropmeta->width = self->crop.width;
    cropmeta->height = self->crop.height;
  }

  return;

add_meta_failed:
  {
    GST_WARNING_OBJECT (self, "Could not add croping information %"
        GST_PTR_FORMAT, buf);
  }
}

/* Prepares the output_buffer of @frame */
static inline GstFlowReturn
prepare_output_buffer (GstDucatiVidDec * self, GstVideoCodecFrame * frame)
{
  GstFlowReturn ret;

  if (G_UNLIKELY (!self->codec)) {
    GST_DEBUG_OBJECT (self, "No codec yet, creating it");
    if (!codec_create (self))
      goto create_codec_fails;
  }

  GST_DEBUG ("Preparing output buffer");
  ret = gst_video_decoder_allocate_output_frame (GST_VIDEO_DECODER (self),
      frame);
  if (ret != GST_FLOW_OK)
    goto alloc_buffer_failed;

  self->inArgs->inputID = codec_prepare_outbuf (self, frame);

  if (!self->inArgs->inputID)
    goto codec_prepare_output_failed;

  return GST_FLOW_OK;

  /* Errors handling */

alloc_buffer_failed:
  GST_WARNING_OBJECT (self, "alloc_buffer failed %s", gst_flow_get_name (ret));
  return ret;

create_codec_fails:
  GST_ERROR_OBJECT (self, "could not create codec");
  return GST_FLOW_ERROR;

codec_prepare_output_failed:
  GST_ERROR_OBJECT (self, "could not prepare output buffer");
  return GST_FLOW_ERROR;
}

static gint
codec_process (GstDucatiVidDec * self, gboolean send,
    gboolean flush, GstFlowReturn * flow_ret)
{
  gint i;
  gint err;
  GstClockTime t;
  GstBuffer *outbuf;
  GstVideoCodecFrame *frame;

  GstFlowReturn ret = GST_FLOW_OK;

  GstDucatiVidDecClass *klass = GST_DUCATIVIDDEC_GET_CLASS (self);

  if (flow_ret)
    /* never leave flow_ret uninitialized */
    *flow_ret = GST_FLOW_OK;

  memset (&self->outArgs->outputID, 0, sizeof (self->outArgs->outputID));
  memset (&self->outArgs->freeBufID, 0, sizeof (self->outArgs->freeBufID));

  GST_DEBUG ("Calling VIDDEC3_process");
  t = gst_util_get_timestamp ();
  err = VIDDEC3_process (self->codec, self->inBufs, self->outBufs,
      self->inArgs, self->outArgs);
  GST_DEBUG_OBJECT (self, "VIDDEC3_process took %10dns (%d ms)", (gint) t,
      (gint) (t / 1000000));


  if (err) {
    GST_WARNING_OBJECT (self, "err=%d, extendedError=%08x",
        err, self->outArgs->extendedError);
    gst_ducati_log_extended_error_info (self->outArgs->extendedError,
        self->error_strings);

    err = VIDDEC3_control (self->codec, XDM_GETSTATUS,
        self->dynParams, self->status);
    if (err) {
      GST_WARNING_OBJECT (self, "XDM_GETSTATUS: err=%d, extendedError=%08x",
          err, self->status->extendedError);
      gst_ducati_log_extended_error_info (self->status->extendedError,
          self->error_strings);
    }

    if (flush)
      err = XDM_EFAIL;
    else
      err = klass->handle_error (self, err,
          self->outArgs->extendedError, self->status->extendedError);
  }

  /* we now let the codec decide */
  self->dynParams->newFrameFlag = XDAS_FALSE;

  if (err == XDM_EFAIL)
    goto skip_outbuf_processing;

  for (i = 0; i < IVIDEO2_MAX_IO_BUFFERS && self->outArgs->outputID[i]; i++) {
    GstClockTime pts;
    GstVideoInterlaceMode interlaced;

    /* Getting an extra reference for the decoder */
    frame = codec_add_frame (self, self->outArgs->outputID[i]);
    if (send == FALSE) {
      /* if send is FALSE:
       *  + Don't try to renegotiate as we could be flushing during a
       *    PAUSED->READY state change
       *  + Do not try to push the frame downstream
       *  + Declare the frames as dropped
       */

      GST_DEBUG_OBJECT (self, "frame not pushed, dropping 'chain' ref: %d %p",
          i, frame);

      ret = gst_video_decoder_drop_frame (GST_VIDEO_DECODER (self), frame);
      continue;
    }

    if (self->outArgs->decodedBufs.contentType == IVIDEO_PROGRESSIVE)
      interlaced = GST_VIDEO_INTERLACE_MODE_PROGRESSIVE;
    else
      interlaced = GST_VIDEO_INTERLACE_MODE_INTERLEAVED;

    if (interlaced != self->output_state->info.interlace_mode) {
      GstVideoCodecState *new_state;

      GST_WARNING_OBJECT (self, "upstream set interlaced=%d but codec "
          "thinks interlaced=%d... trusting codec... and droping frame",
          self->output_state->info.interlace_mode, interlaced);

      self->output_state->info.interlace_mode = interlaced;
      /* VideoDecoder will take care of renegotiating */
      new_state = gst_video_decoder_set_output_state (GST_VIDEO_DECODER (self),
          GST_VIDEO_FORMAT_NV12, self->padded_width, self->padded_height,
          self->output_state);
      gst_video_codec_state_unref (self->output_state);
      self->output_state = new_state;

      /* this buffer still has the old config so we drop it */
      ret = gst_video_decoder_drop_frame (GST_VIDEO_DECODER (self), frame);
      /* and the following ones */
      send = FALSE;

      continue;
    }

    outbuf = frame->output_buffer;
    pts = frame->pts;
    GST_DEBUG_OBJECT (self, "got buffer: %d %p (%" GST_TIME_FORMAT ")",
        i, outbuf, GST_TIME_ARGS (pts));

#ifdef USE_DTS_PTS_CODE
    if (self->ts_may_be_pts) {
      if ((self->last_pts != GST_CLOCK_TIME_NONE) && (self->last_pts > pts)) {
        GST_DEBUG_OBJECT (self, "detected PTS going backwards, "
            "enabling ts_is_pts");
        self->ts_is_pts = TRUE;
      }
    }
#endif

    self->last_pts = pts;

    if (self->dts_ridx != self->dts_widx) {
      pts = self->dts_queue[self->dts_ridx++ % NDTS];
    }

    if (self->ts_is_pts) {
      /* if we have a queued DTS from demuxer, use that instead: */
      frame->pts = pts;
      GST_DEBUG_OBJECT (self, "fixed pts: %d %p (%" GST_TIME_FORMAT ")",
          i, outbuf, GST_TIME_ARGS (pts));
    }

    add_crop_meta (self, outbuf);

    GST_DEBUG_OBJECT (self, "Done with frame %p", self->inArgs->inputID);
    ret = gst_video_decoder_finish_frame (GST_VIDEO_DECODER (self), frame);
    if (flow_ret)
      *flow_ret = ret;

    if (ret != GST_FLOW_OK) {
      GST_WARNING_OBJECT (self, "push failed %s", gst_flow_get_name (ret));
      /* just unref the remaining buffers (if any) */
      send = FALSE;
    }
  }

skip_outbuf_processing:
  for (i = 0; i < IVIDEO2_MAX_IO_BUFFERS && self->outArgs->freeBufID[i]; i++)
    codec_unlock_frame (self, self->outArgs->freeBufID[i]);

  return err;
}

/** call control(FLUSH), and then process() to pop out all buffers */
static gboolean
gst_ducati_viddec_codec_flush (GstDucatiVidDec * self, gboolean hard,
    gboolean eos)
{
  gint err = FALSE;
  int prev_num_in_bufs, prev_num_out_bufs;

  GST_DEBUG_OBJECT (self, "flush: eos=%d", eos);

  /* FIXME GstVideoDecoder: Here we should discont */
  /* note: flush is synchronized against _chain() to avoid calling
   * the codec from multiple threads
   */
  GST_PAD_STREAM_LOCK (GST_VIDEO_DECODER_SRC_PAD (self));

#ifdef USE_DTS_PTS_CODE
  self->dts_ridx = self->dts_widx = 0;
  self->last_dts = self->last_pts = GST_CLOCK_TIME_NONE;
  self->ts_may_be_pts = TRUE;
  self->ts_is_pts = FALSE;
#endif
  self->wait_keyframe = TRUE;
  self->in_size = 0;
  self->needs_flushing = FALSE;
  self->need_out_buf = TRUE;

  if (G_UNLIKELY (self->first_in_buffer)) {
    goto out;
  }

  if (G_UNLIKELY (!self->codec)) {
    GST_WARNING_OBJECT (self, "no codec");
    goto out;
  }

  err = VIDDEC3_control (self->codec, XDM_FLUSH, self->dynParams, self->status);
  if (err) {
    GST_ERROR_OBJECT (self, "failed XDM_FLUSH");
    goto out;
  }

  prev_num_in_bufs = self->inBufs->numBufs;
  prev_num_out_bufs = self->outBufs->numBufs;

  self->inBufs->descs[0].bufSize.bytes = 0;
  self->inBufs->numBufs = 0;
  self->inArgs->numBytes = 0;
  self->inArgs->inputID = 0;
  self->outBufs->numBufs = 0;

  do {
    err = codec_process (self, eos, TRUE, NULL);
  } while (err != XDM_EFAIL);

  /* We flushed the decoder, we can now remove the buffer that have never been
   * unrefed in it */
  g_hash_table_remove_all (self->passed_in_bufs);

  /* reset outArgs in case we're flushing in codec_process trying to do error
   * recovery */
  memset (&self->outArgs->outputID, 0, sizeof (self->outArgs->outputID));
  memset (&self->outArgs->freeBufID, 0, sizeof (self->outArgs->freeBufID));

  self->dynParams->newFrameFlag = XDAS_TRUE;

  /* Reset the push buffer and YUV buffers, plus any codec specific buffers */
  self->inBufs->numBufs = prev_num_in_bufs;
  self->outBufs->numBufs = prev_num_out_bufs;

  /* on a flush, it is normal (and not an error) for the last _process() call
   * to return an error..
   */
  err = XDM_EOK;

out:
  GST_PAD_STREAM_UNLOCK (GST_VIDEO_DECODER_SRC_PAD (self));
  GST_DEBUG_OBJECT (self, "done");

  return !err;
}

/**
 * perform qos calculations before decoding the next frame.
 *
 * Returns: %TRUE if the frame should be decoded, %FALSE if the frame can be
 * dropped entirely.
 */
static gboolean
gst_ducati_viddec_do_qos (GstDucatiVidDec * self, GstVideoCodecFrame * frame)
{
  GstClockTimeDiff diff;

  if (self->wait_keyframe) {
    if (!GST_VIDEO_CODEC_FRAME_IS_SYNC_POINT (frame)) {
      GST_INFO_OBJECT (self, "skipping until the next keyframe");
      return FALSE;
    }

    self->wait_keyframe = FALSE;
  }

  diff = gst_video_decoder_get_max_decode_time (GST_VIDEO_DECODER (self),
      frame);

  /* if we don't have timing info, then we don't do QoS */
  if (G_UNLIKELY (!GST_CLOCK_TIME_IS_VALID (diff)))
    return TRUE;

  if (diff < 0 && !GST_VIDEO_CODEC_FRAME_IS_SYNC_POINT (frame)) {
    GST_INFO_OBJECT (self, "dropping frame, diff %i", diff);

    return FALSE;
  }

  return TRUE;
}

/**********************************************************
 *                                                        *
 *     GstDucatiVidDec vmethod default implementations    *
 *                                                        *
 **********************************************************/
static gboolean
gst_ducati_viddec_allocate_params (GstDucatiVidDec * self, gint params_sz,
    gint dynparams_sz, gint status_sz, gint inargs_sz, gint outargs_sz)
{

  /* allocate params: */
  self->params = dce_alloc (params_sz);
  if (G_UNLIKELY (!self->params)) {
    return FALSE;
  }
  self->params->size = params_sz;
  self->params->maxFrameRate = 30000;
  self->params->maxBitRate = 10000000;

  self->params->dataEndianness = XDM_BYTE;
  self->params->forceChromaFormat = XDM_YUV_420SP;
  self->params->operatingMode = IVIDEO_DECODE_ONLY;

  self->params->displayBufsMode = IVIDDEC3_DISPLAYBUFS_EMBEDDED;
  self->params->inputDataMode = IVIDEO_ENTIREFRAME;
  self->params->outputDataMode = IVIDEO_ENTIREFRAME;
  self->params->numInputDataUnits = 0;
  self->params->numOutputDataUnits = 0;

  self->params->metadataType[0] = IVIDEO_METADATAPLANE_NONE;
  self->params->metadataType[1] = IVIDEO_METADATAPLANE_NONE;
  self->params->metadataType[2] = IVIDEO_METADATAPLANE_NONE;
  self->params->errorInfoMode = IVIDEO_ERRORINFO_OFF;

  /* Let the decoder reorder frame */
  self->params->displayDelay = IVIDDEC3_DISPLAY_DELAY_AUTO;

  /* allocate dynParams: */
  self->dynParams = dce_alloc (dynparams_sz);
  if (G_UNLIKELY (!self->dynParams)) {
    return FALSE;
  }
  self->dynParams->size = dynparams_sz;
  self->dynParams->decodeHeader = XDM_DECODE_AU;
  self->dynParams->displayWidth = 0;
  self->dynParams->frameSkipMode = IVIDEO_NO_SKIP;
  self->dynParams->newFrameFlag = XDAS_TRUE;

  /* allocate status: */
  self->status = dce_alloc (status_sz);
  if (G_UNLIKELY (!self->status)) {
    return FALSE;
  }
  memset (self->status, 0, status_sz);
  self->status->size = status_sz;

  /* allocate inBufs/outBufs: */
  self->inBufs = dce_alloc (sizeof (XDM2_BufDesc));
  self->outBufs = dce_alloc (sizeof (XDM2_BufDesc));
  if (G_UNLIKELY (!self->inBufs) || G_UNLIKELY (!self->outBufs)) {
    return FALSE;
  }

  /* allocate inArgs/outArgs: */
  self->inArgs = dce_alloc (inargs_sz);
  self->outArgs = dce_alloc (outargs_sz);
  if (G_UNLIKELY (!self->inArgs) || G_UNLIKELY (!self->outArgs)) {
    return FALSE;
  }
  self->inArgs->size = inargs_sz;
  self->outArgs->size = outargs_sz;

  return TRUE;
}

static GstFlowReturn
gst_ducati_viddec_push_input (GstDucatiVidDec * self,
    GstVideoCodecFrame * frame)
{
  GstBuffer *buf = frame->input_buffer;

  if (G_UNLIKELY (self->first_in_buffer) && self->input_state->codec_data)
    push_input_buffer (self, self->input_state->codec_data, 0);

  /* just copy entire buffer */
  push_input_buffer (self, buf, 0);

  return GST_FLOW_OK;
}

static gint
gst_ducati_viddec_handle_error (GstDucatiVidDec * self, gint ret,
    gint extended_error, gint status_extended_error)
{
  if (XDM_ISFATALERROR (extended_error))
    ret = XDM_EFAIL;
  else
    ret = XDM_EOK;

  return ret;
}

/********************************************
 *                                          *
 * GstVideoDecoder vmethods implementation  *
 *                                          *
 ********************************************/
static gboolean
ducati_dec_open (GstVideoDecoder * decoder)
{
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (decoder);
  gboolean supported;

  if (!engine_open (self)) {
    GST_ERROR_OBJECT (self, "could not open");
    return FALSE;
  }
  /* try to create/destroy the codec here, it may not be supported */
  supported = codec_create (self);
  codec_delete (self);
  self->codec = NULL;
  if (!supported) {
    GST_ERROR_OBJECT (self, "Failed to create codec %s, not supported",
        GST_DUCATIVIDDEC_GET_CLASS (self)->codec_name);
    engine_close (self);
    return FALSE;
  }

  return TRUE;
}

static gboolean
ducati_dec_set_format (GstVideoDecoder * decoder, GstVideoCodecState * state)
{
  gint w, h;
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (decoder);
  GstVideoCodecState *cstate = self->input_state ? self->input_state : state;


  h = ALIGN2 (cstate->info.height, 4);  /* round up to MB */
  w = ALIGN2 (cstate->info.width, 4);   /* round up to MB */

  /* if we've already created codec, but the resolution has changed, we
   * need to re-create the codec: */
  if (G_UNLIKELY (self->codec)) {
    if ((h != self->height) || (w != self->width)) {
      codec_delete (self);
    }
  }

  self->width = w;
  self->height = h;

  /* (re)send a crop event when caps change */
  self->needs_crop_calculation = TRUE;

  /* And keep a ref to the input state */
  if (self->input_state)
    gst_video_codec_state_unref (self->input_state);

  self->input_state = gst_video_codec_state_ref (state);

  /* Now set the ouptut state */
  g_assert (GST_DUCATIVIDDEC_GET_CLASS (self)->update_buffer_size);
  GST_DUCATIVIDDEC_GET_CLASS (self)->update_buffer_size (self);
  self->output_state =
      gst_video_decoder_set_output_state (decoder, GST_VIDEO_FORMAT_NV12,
      self->padded_width, self->padded_height, state);

  return TRUE;
}

static gboolean
ducati_decide_allocation (GstVideoDecoder * decoder, GstQuery * query)
{
  GstCaps *outcaps;
  GstStructure *config;

  GstBufferPool *pool = NULL;
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (decoder);
  guint nb_pools, min = self->min_buffers, max = 0, size = 0;

  if (!GST_VIDEO_DECODER_CLASS (parent_class)->decide_allocation (decoder,
          query))
    return FALSE;

  gst_query_parse_allocation (query, &outcaps, NULL);
  if ((nb_pools = gst_query_get_n_allocation_pools (query))) {
    guint i;

    for (i = 0; i < nb_pools; i++) {
      gst_query_parse_nth_allocation_pool (query, i, &pool, &size, &min, &max);

      GST_DEBUG_OBJECT (pool, "proposed in query %p", query);
      /* Check that it has the proper options */
      if (gst_buffer_pool_has_option (pool, GST_BUFFER_POOL_OPTION_DRM_META) &&
          gst_buffer_pool_has_option (pool, GST_BUFFER_POOL_OPTION_DMABUF_META)
          && gst_buffer_pool_has_option (pool,
              GST_BUFFER_POOL_OPTION_VIDEO_META))
        break;

      gst_object_unref (pool);
      pool = NULL;
    }

    if (min < self->min_buffers)
      min = self->min_buffers;
  }

  if (self->pool && (pool == self->pool)) {
    GST_DEBUG_OBJECT (self, "Proposing the same pool, keeping it");
    gst_object_unref (pool);
    goto done;
  }

  if (pool) {
    GST_DEBUG_OBJECT (self, "Using proposed pool: %" GST_PTR_FORMAT, pool);
    self->pool = pool;
    goto done;
  } else {
    GST_DEBUG_OBJECT (self, "Creating new pool");
    self->pool = gst_drm_buffer_pool_new (dce_get_fd ());
  }

done:
  if (size < self->output_state->info.size)
    size = self->output_state->info.size;

  config = gst_buffer_pool_get_config (self->pool);
  gst_buffer_pool_config_add_option (config, GST_BUFFER_POOL_OPTION_DRM_META);
  gst_buffer_pool_config_add_option (config, GST_BUFFER_POOL_OPTION_VIDEO_META);
  gst_buffer_pool_config_add_option (config,
      GST_BUFFER_POOL_OPTION_DMABUF_META);
  gst_buffer_pool_config_set_params (config, self->output_state->caps, size,
      min, max);
  gst_buffer_pool_set_config (self->pool, config);

  /* Give our pool as best option */
  if (nb_pools)
    gst_query_set_nth_allocation_pool (query, 0, self->pool, size, min, max);
  else
    gst_query_add_allocation_pool (query, self->pool, size, min, max);

  return TRUE;
}

static GstFlowReturn
ducati_dec_handle_frame (GstVideoDecoder * decoder, GstVideoCodecFrame * frame)
{
  Int32 err;

  GstFlowReturn ret = GST_FLOW_OK;

  GstBuffer *buf = frame->input_buffer;
  GstClockTime ts = frame->dts;
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (decoder);

  if (G_UNLIKELY (!self->engine)) {
    GST_ERROR_OBJECT (self, "no engine");

    return gst_video_decoder_drop_frame (decoder, frame);
  }

  GST_DEBUG_OBJECT (self, "Handle frame %p, time: %" GST_TIME_FORMAT
      " (%d bytes, flags %d)", frame,
      GST_TIME_ARGS (GST_BUFFER_PTS (frame->input_buffer)),
      gst_buffer_get_size (buf), GST_BUFFER_FLAGS (buf));

  if (!gst_ducati_viddec_do_qos (self, frame))
    return gst_video_decoder_drop_frame (decoder, frame);

  if (self->need_out_buf) {
    ret = prepare_output_buffer (self, frame);

    if (ret != GST_FLOW_OK) {
      GST_WARNING_OBJECT (self, "Could not prepare output buffer: %s",
          gst_flow_get_name (ret));
      return ret;
    }
  }

  /* Now push buffer into the decoder */
  ret = GST_DUCATIVIDDEC_GET_CLASS (self)->push_input (self, frame);
  if (ret != GST_FLOW_OK) {
    GST_WARNING_OBJECT (self, "Could not push input buffer buffer: %s",
        gst_flow_get_name (ret));
    return ret;
  }

  /* FIXME: Check that we do NOT need that hack anymore with 1.0 and the
   * clear difference between DTS and PTS in core*/
#ifdef USE_DTS_PTS_CODE
  if (ts != GST_CLOCK_TIME_NONE) {
    self->dts_queue[self->dts_widx++ % NDTS] = ts;
    /* if next buffer has earlier ts than previous, then the ts
     * we are getting are definitely decode order (DTS):
     */
    if ((self->last_dts != GST_CLOCK_TIME_NONE) && (self->last_dts > ts)) {
      GST_DEBUG_OBJECT (self, "input timestamp definitely DTS");
      self->ts_may_be_pts = FALSE;
    }
    self->last_dts = ts;
  }
#endif

  if (self->in_size == 0 && frame->output_buffer) {
    GST_DEBUG_OBJECT (self, "no input, skipping process");

    return gst_video_decoder_drop_frame (decoder, frame);
  }

  self->inArgs->numBytes = self->in_size;
  self->inBufs->descs[0].bufSize.bytes = self->in_size;
  self->inBufs->descs[0].memType = XDM_MEMTYPE_BO;

  err = codec_process (self, TRUE, FALSE, &ret);
  if (err) {
    GST_ELEMENT_ERROR (self, STREAM, DECODE, (NULL),
        ("process returned error: %d %08x", err, self->outArgs->extendedError));
    gst_ducati_log_extended_error_info (self->outArgs->extendedError,
        self->error_strings);

    return GST_FLOW_ERROR;
  }

  if (ret != GST_FLOW_OK) {
    GST_WARNING_OBJECT (self, "push from codec_process failed %s",
        gst_flow_get_name (ret));

    /* codec_proccess failed, skip current data */
    self->in_size = 0;
    return ret;
  }

  self->first_in_buffer = FALSE;

  if (self->params->inputDataMode != IVIDEO_ENTIREFRAME) {
    /* FIXME GstVideoDecoder Do we still need to support other modes ?? */

    /* The copy could be avoided by playing with the buffer pointer,
       but it seems to be rare and for not many bytes */
    GST_DEBUG_OBJECT (self, "Consumed %d/%d (%d) bytes, %d left",
        self->outArgs->bytesConsumed, self->in_size,
        self->inArgs->numBytes, self->in_size - self->outArgs->bytesConsumed);
    if (self->outArgs->bytesConsumed > 0) {
      if (self->outArgs->bytesConsumed > self->in_size) {
        GST_WARNING_OBJECT (self,
            "Codec claims to have used more bytes than supplied");
        self->in_size = 0;
      } else {
        if (self->outArgs->bytesConsumed < self->in_size) {
          memmove (self->input, self->input + self->outArgs->bytesConsumed,
              self->in_size - self->outArgs->bytesConsumed);
        }
        self->in_size -= self->outArgs->bytesConsumed;
      }
    }
  } else {
    self->in_size = 0;
  }

  if (self->outArgs->outBufsInUseFlag) {
    GST_DEBUG_OBJECT (self, "outBufsInUseFlag set");
    self->need_out_buf = FALSE;
  } else {
    self->need_out_buf = TRUE;
  }

  /* FIXME GstVideoDecoder: What should be done Here?
     if (self->needs_flushing)
     gst_video_decoder_reset (self, FALSE, FALSE);
   */

  return GST_FLOW_OK;
}

static gboolean
ducati_dec_reset (GstVideoDecoder * decoder, gboolean hard)
{
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (decoder);

  /* FIXME GstVideoDecoder what should be done about *hard* ? */
  return gst_ducati_viddec_codec_flush (self, hard, FALSE);
}

static GstFlowReturn
ducati_dec_finish (GstVideoDecoder * decoder)
{
  return gst_ducati_viddec_codec_flush (GST_DUCATIVIDDEC (decoder), FALSE,
      TRUE);
}

static gboolean
ducati_dec_stop (GstVideoDecoder * decoder)
{
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (decoder);

  self->needs_crop_calculation = TRUE;
  gst_ducati_viddec_codec_flush (self, TRUE, FALSE);

  return TRUE;
}

static gboolean
ducati_dec_close (GstVideoDecoder * decoder)
{
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (decoder);

  codec_delete (self);
  engine_close (self);

  return TRUE;
}

/********************************************
 *                                          *
 *     GObject vmethod implementations      *
 *                                          *
 ********************************************/
#define VERSION_LENGTH 256

static void
gst_ducati_viddec_get_property (GObject * obj,
    guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (obj);


  switch (prop_id) {
    case PROP_VERSION:
    {
      int err;
      char *version = NULL;

      if (!self->engine)
        engine_open (self);

      if (!self->codec)
        codec_create (self);

      if (self->codec) {
        version = dce_alloc (VERSION_LENGTH);
        self->status->data.buf = (XDAS_Int8 *) version;
        self->status->data.bufSize = VERSION_LENGTH;

        err = VIDDEC3_control (self->codec, XDM_GETVERSION,
            self->dynParams, self->status);
        if (err) {
          GST_ERROR_OBJECT (self, "failed XDM_GETVERSION");
        }

        self->status->data.buf = NULL;
        self->status->data.bufSize = 0;
      }

      g_value_set_string (value, version);
      if (version)
        dce_free (version);

      break;
    }
    case PROP_CODEC_DEBUG_INFO:
      g_value_set_boolean (value, self->codec_debug_info);
      break;
    default:{
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
      break;
    }
  }
}

static void
gst_ducati_viddec_set_property (GObject * obj,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (obj);

  switch (prop_id) {
    case PROP_CODEC_DEBUG_INFO:
      self->codec_debug_info = g_value_get_boolean (value);
      break;
    default:{
      G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
      break;
    }
  }
}

static void
gst_ducati_viddec_finalize (GObject * obj)
{
  GstDucatiVidDec *self = GST_DUCATIVIDDEC (obj);

  codec_delete (self);
  engine_close (self);

  /* Will unref the remaining buffers if needed */
  g_hash_table_unref (self->passed_in_bufs);
  if (self->input_state) {
    gst_video_codec_state_unref (self->input_state);
    self->input_state = NULL;
  }

  if (self->output_state) {
    gst_video_codec_state_unref (self->output_state);
    self->output_state = NULL;
  }

  G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void
gst_ducati_viddec_class_init (GstDucatiVidDecClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);
  GstVideoDecoderClass *video_decoder_class = GST_VIDEO_DECODER_CLASS (klass);

  gobject_class->get_property =
      GST_DEBUG_FUNCPTR (gst_ducati_viddec_get_property);
  gobject_class->set_property =
      GST_DEBUG_FUNCPTR (gst_ducati_viddec_set_property);
  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_ducati_viddec_finalize);

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_factory));

  video_decoder_class->open = GST_DEBUG_FUNCPTR (ducati_dec_open);
  video_decoder_class->set_format = GST_DEBUG_FUNCPTR (ducati_dec_set_format);
  video_decoder_class->handle_frame =
      GST_DEBUG_FUNCPTR (ducati_dec_handle_frame);
  video_decoder_class->decide_allocation =
      GST_DEBUG_FUNCPTR (ducati_decide_allocation);
  video_decoder_class->reset = GST_DEBUG_FUNCPTR (ducati_dec_reset);
  video_decoder_class->finish = GST_DEBUG_FUNCPTR (ducati_dec_finish);
  video_decoder_class->stop = GST_DEBUG_FUNCPTR (ducati_dec_stop);
  video_decoder_class->close = GST_DEBUG_FUNCPTR (ducati_dec_close);

  klass->allocate_params =
      GST_DEBUG_FUNCPTR (gst_ducati_viddec_allocate_params);
  klass->push_input = GST_DEBUG_FUNCPTR (gst_ducati_viddec_push_input);
  klass->handle_error = GST_DEBUG_FUNCPTR (gst_ducati_viddec_handle_error);
  klass->update_buffer_size = NULL;

  g_object_class_install_property (gobject_class, PROP_VERSION,
      g_param_spec_string ("version", "Version",
          "The codec version string", "",
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CODEC_DEBUG_INFO,
      g_param_spec_boolean ("codec-debug-info",
          "Gather debug info from the codec",
          "Gather and log relevant debug information from the codec. "
          "What is gathered is typically codec specific", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static void
gst_ducati_viddec_init (GstDucatiVidDec * self)
{
  /* sane defaults in case we need to  create codec without caps negotiation
   * (for example, to get 'version' property)
   */
  self->width = 128;
  self->height = 128;

  self->first_in_buffer = TRUE;
  self->needs_crop_calculation = TRUE;

#ifdef USE_DTS_PTS_CODE
  self->dts_ridx = self->dts_widx = 0;
  self->last_dts = self->last_pts = GST_CLOCK_TIME_NONE;
  self->ts_may_be_pts = TRUE;
  self->ts_is_pts = FALSE;
#endif

  self->pageMemType = XDM_MEMTYPE_TILEDPAGE;

  self->wait_keyframe = TRUE;
  self->need_out_buf = TRUE;
  self->device = NULL;
  self->input_bo = NULL;

  self->passed_in_bufs = g_hash_table_new_full (g_direct_hash, g_direct_equal,
      NULL, (GDestroyNotify) gst_video_codec_frame_unref);
}
