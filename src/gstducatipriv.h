/*
 * GStreamer
 *
 * Copyright (C) 2012 Texas Instruments
 * Copyright (C) 2012 Collabora Ltd
 *
 * Authors:
 *  Alessandro Decina <alessandro.decina@collabora.co.uk>
 *  Rob Clark <rob.clark@linaro.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __GSTDUCATIPRIV_H__
#define __GSTDUCATIPRIV_H__

#include <stdint.h>
#include <gst/gst.h>
#include <omap_drm.h>
#include <omap_drmif.h>
#include <gst/video/gstvideoutils.h>

G_BEGIN_DECLS

/*
 * per-buffer private data for gst-ducati
 *
 * We are not using GstMeta as this is not meat to be shared with
 * other elements
 */

GST_EXPORT GType _gst_ducati_priv_type;

#define GST_TYPE_DUCATI_PRIV (_gst_ducati_priv)
#define GST_DUCATI_PRIV(obj) ((GstDucatiPriv*) obj)
#define GST_IS_DUCATI_PRIV(obj) GST_IS_MINI_OBJECT_TYPE(obj, GST_TYPE_DUCATI_PRIV)

typedef struct _GstDucatiPriv      GstDucatiPriv;
struct _GstDucatiPriv
{
  GstMiniObject parent;

  struct omap_bo *bo;
  gint            uv_offset;
  gint            size;
};

GType gst_ducati_priv_get_type (void);
GstDucatiPriv * gst_buffer_add_ducati_priv (GstBuffer * buffer,
                                            struct omap_device *device,
                                            GstVideoInfo * info);

G_END_DECLS


#endif /* __GSTDUCATIPRIV_H__ */
