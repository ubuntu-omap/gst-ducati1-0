/* GStreamer
 * Copyright (c) 2011, Texas Instruments Incorporated
 * Copyright (c) 2011, Collabora Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Alessandro Decina <alessandro.decina@collabora.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstducati.h"
#include "gstducatipriv.h"
#include "gstducatividenc.h"

#include <math.h>
#include <string.h>
#include <sys/drm/gstdrmmeta.h>
#include <sys/dmabuf/gstdmabufmeta.h>


#define GST_CAT_DEFAULT gst_ducati_debug
GST_DEBUG_CATEGORY_STATIC (GST_CAT_PERFORMANCE);

#define DEFAULT_BITRATE 2048
#define DEFAULT_RATE_PRESET GST_DUCATI_VIDENC_RATE_PRESET_STORAGE
#define DEFAULT_INTRA_INTERVAL 16

#define GST_TYPE_DUCATI_VIDENC_RATE_PRESET (gst_ducati_videnc_rate_preset_get_type ())


enum
{
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_BITRATE,
  PROP_RATE_PRESET,
  PROP_INTRA_INTERVAL
};

#define gst_ducati_videnc_parent_class parent_class
G_DEFINE_TYPE (GstDucatiVidEnc, gst_ducati_videnc, GST_TYPE_VIDEO_ENCODER);

/* the values for the following enums are taken from the codec */
enum
{
  GST_DUCATI_VIDENC_RATE_PRESET_LOW_DELAY = IVIDEO_LOW_DELAY,   /**< CBR rate control for video conferencing. */
  GST_DUCATI_VIDENC_RATE_PRESET_STORAGE = IVIDEO_STORAGE,  /**< VBR rate control for local storage (DVD)
                           *   recording.
                           */
  GST_DUCATI_VIDENC_RATE_PRESET_TWOPASS = IVIDEO_TWOPASS,  /**< Two pass rate control for non real time
                           *   applications.
                           */
  GST_DUCATI_VIDENC_RATE_PRESET_NONE = IVIDEO_NONE,        /**< No configurable video rate control
                            *  mechanism.
                            */
  GST_DUCATI_VIDENC_RATE_PRESET_USER_DEFINED = IVIDEO_USER_DEFINED,/**< User defined configuration using extended
                           *   parameters.
                           */
};

static GType
gst_ducati_videnc_rate_preset_get_type (void)
{
  static GType type = 0;

  if (!type) {
    static const GEnumValue vals[] = {
      {GST_DUCATI_VIDENC_RATE_PRESET_LOW_DELAY, "Low Delay", "low-delay"},
      {GST_DUCATI_VIDENC_RATE_PRESET_STORAGE, "Storage", "storage"},
      {GST_DUCATI_VIDENC_RATE_PRESET_TWOPASS, "Two-Pass", "two-pass"},
      {GST_DUCATI_VIDENC_RATE_PRESET_NONE, "None", "none"},
      {GST_DUCATI_VIDENC_RATE_PRESET_USER_DEFINED, "User defined",
          "user-defined"},
      {0, NULL, NULL},
    };

    type = g_enum_register_static ("GstDucatiVidEncRatePreset", vals);
  }

  return type;
}

static void
apply_cropping (GstDucatiVidEnc * self, GstVideoCodecFrame * frame)
{
  GstVideoCropMeta *crop;

  crop = gst_buffer_get_video_crop_meta (frame->input_buffer);

  if (crop) {
    /* setting imageRegion doesn't seem to be strictly needed if activeFrameRegion
     * is set but we set it anyway...  */
    self->inBufs->activeFrameRegion.topLeft.x =
        self->inBufs->imageRegion.topLeft.x = crop->x;
    self->inBufs->activeFrameRegion.topLeft.y =
        self->inBufs->imageRegion.topLeft.y = crop->y;
    self->inBufs->activeFrameRegion.bottomRight.x =
        self->inBufs->imageRegion.bottomRight.x = crop->x + crop->width;
    self->inBufs->activeFrameRegion.bottomRight.y =
        self->inBufs->imageRegion.bottomRight.y = crop->y + crop->height;

    return;
  }

  self->inBufs->activeFrameRegion.topLeft.x =
      self->inBufs->imageRegion.topLeft.x = 0;
  self->inBufs->activeFrameRegion.topLeft.y =
      self->inBufs->imageRegion.topLeft.y = 0;
  self->inBufs->activeFrameRegion.bottomRight.x =
      self->inBufs->imageRegion.bottomRight.x = self->input_state->info.width;
  self->inBufs->activeFrameRegion.bottomRight.y =
      self->inBufs->imageRegion.bottomRight.y = self->input_state->info.height;
}

static gboolean
ducati_videnc_set_format (GstVideoEncoder * video_encoder,
    GstVideoCodecState * input_state)
{
  GstDucatiVidEnc *self = GST_DUCATIVIDENC (video_encoder);

  /* Store input input_state */
  if (self->input_state)
    gst_video_codec_state_unref (self->input_state);
  self->input_state = gst_video_codec_state_ref (input_state);

  self->configure = TRUE;

  return TRUE;
}

static void
gst_ducati_videnc_finalize (GObject * object)
{
  GstDucatiVidEnc *self;

  g_return_if_fail (GST_IS_DUCATIVIDENC (object));
  self = GST_DUCATIVIDENC (object);

  if (self->input_state)
    gst_video_codec_state_unref (self->input_state);
  if (self->output_state)
    gst_video_codec_state_unref (self->output_state);
}

static void
gst_ducati_videnc_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstDucatiVidEnc *self;

  g_return_if_fail (GST_IS_DUCATIVIDENC (object));
  self = GST_DUCATIVIDENC (object);

  switch (prop_id) {
    case PROP_BITRATE:
      self->bitrate = g_value_get_int (value) * 1000;
      break;
    case PROP_RATE_PRESET:
      self->rate_preset = g_value_get_enum (value);
      break;
    case PROP_INTRA_INTERVAL:
      self->intra_interval = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
gst_ducati_videnc_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstDucatiVidEnc *self;

  g_return_if_fail (GST_IS_DUCATIVIDENC (object));
  self = GST_DUCATIVIDENC (object);

  switch (prop_id) {
    case PROP_BITRATE:
      g_value_set_int (value, self->bitrate / 1000);
      break;
    case PROP_RATE_PRESET:
      g_value_set_enum (value, self->rate_preset);
      break;
    case PROP_INTRA_INTERVAL:
      g_value_set_int (value, self->intra_interval);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static gboolean
gst_ducati_videnc_configure (GstDucatiVidEnc * self, GstVideoCodecFrame * frame)
{
  int err, i;
  GstVideoCropMeta *crop;
  VIDENC2_Params *params;
  VIDENC2_DynamicParams *dynParams;

  GstCaps *caps = NULL;
  int max_out_size = 0;
  GstVideoInfo *info = &self->input_state->info;

  params = (VIDENC2_Params *) self->params;
  params->encodingPreset = 0x03;
  params->rateControlPreset = self->rate_preset;
  params->dataEndianness = XDM_BYTE;
  params->maxInterFrameInterval = 1;
  params->maxBitRate = -1;
  params->minBitRate = 0;
  params->maxHeight = info->height;
  params->maxWidth = info->width;
  params->inputChromaFormat = XDM_YUV_420SP;
  params->inputContentType = IVIDEO_PROGRESSIVE;
  params->operatingMode = IVIDEO_ENCODE_ONLY;
  params->inputDataMode = IVIDEO_ENTIREFRAME;
  params->outputDataMode = IVIDEO_ENTIREFRAME;
  params->numInputDataUnits = 1;
  params->numOutputDataUnits = 1;
  for (i = 0; i < IVIDEO_MAX_NUM_METADATA_PLANES; i++) {
    params->metadataType[i] = IVIDEO_METADATAPLANE_NONE;
  }

  dynParams = (VIDENC2_DynamicParams *) self->dynParams;

  dynParams->refFrameRate = gst_util_uint64_scale (1000, info->fps_n,
      info->fps_d);
  dynParams->targetFrameRate = dynParams->refFrameRate;

  if ((crop = gst_buffer_get_video_crop_meta (frame->input_buffer))) {
    dynParams->inputWidth = crop->width;
    dynParams->inputHeight = crop->height;
  } else {
    dynParams->inputWidth = info->width;
    dynParams->inputHeight = info->height;
  }
  dynParams->targetBitRate = self->bitrate;
  dynParams->intraFrameInterval = self->intra_interval;
  dynParams->captureWidth = dynParams->inputWidth;

  dynParams->forceFrame = IVIDEO_NA_FRAME;
  dynParams->interFrameInterval = 1;
  dynParams->mvAccuracy = IVIDENC2_MOTIONVECTOR_QUARTERPEL;
  dynParams->sampleAspectRatioHeight = 1;
  dynParams->sampleAspectRatioWidth = 1;
  dynParams->generateHeader = XDM_ENCODE_AU;
  dynParams->ignoreOutbufSizeFlag = 1;
  dynParams->lateAcquireArg = -1;

  self->inBufs->chromaFormat = XDM_YUV_420SP;
  self->inBufs->numPlanes = 2;

  g_assert (GST_DUCATIVIDENC_GET_CLASS (self)->configure);
  if (!GST_DUCATIVIDENC_GET_CLASS (self)->configure (self, &caps) ||
      caps == NULL)
    goto configure_failed;

  if (self->codec == NULL) {
    const gchar *codec_name;

    codec_name = GST_DUCATIVIDENC_GET_CLASS (self)->codec_name;
    self->codec = VIDENC2_create (self->engine,
        (String) codec_name, self->params);

    if (self->codec == NULL)
      goto create_codec_fails;
  }

  err = VIDENC2_control (self->codec, XDM_SETPARAMS, self->dynParams,
      self->status);
  if (err) {
    GST_ERROR_OBJECT (self, "XDM_SETPARAMS err=%d, extendedError=%08x",
        err, self->status->extendedError);
    gst_ducati_log_extended_error_info (self->status->extendedError,
        self->error_strings);

    return FALSE;
  }

  err = VIDENC2_control (self->codec, XDM_GETBUFINFO, self->dynParams,
      self->status);
  if (err) {
    GST_ERROR_OBJECT (self, "XDM_GETBUFINFO err=%d, extendedError=%08x",
        err, self->status->extendedError);

    return FALSE;
  }

  self->outBufs->numBufs = self->status->bufInfo.minNumOutBufs;
  for (i = 0; i < self->outBufs->numBufs; i++) {
    int size = self->status->bufInfo.minOutBufSize[i].bytes;
    if (size > max_out_size)
      max_out_size = size;
  }

  self->output_state =
      gst_video_encoder_set_output_state (GST_VIDEO_ENCODER (self), caps,
      self->input_state);
  self->output_state->info.size = max_out_size;

  /* And negotiate the pool */
  if (!gst_video_encoder_negotiate (GST_VIDEO_ENCODER (self)))
    goto negotiation_failed;

  GST_INFO_OBJECT (self, "configured");

  self->configure = FALSE;

  return TRUE;

  /* ERRORS */
configure_failed:
  GST_ERROR_OBJECT (self, "Can not configure subclasses");
  return FALSE;

create_codec_fails:
  GST_ERROR_OBJECT (self, "could not create codec");
  return FALSE;

negotiation_failed:
  GST_WARNING_OBJECT (self, "Could not negotiate pool");
  return FALSE;
}

static gboolean
gst_ducati_videnc_open_engine (GstDucatiVidEnc * self)
{
  int error_code;

  if (self->device == NULL) {
    self->device = dce_init ();
    if (self->device == NULL)
      return FALSE;
  }

  self->engine = Engine_open ((String) "ivahd_vidsvr", NULL, &error_code);
  if (self->engine == NULL) {
    GST_ERROR_OBJECT (self, "couldn't open engine");
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_ducati_videnc_allocate_params (GstDucatiVidEnc * self)
{
  return GST_DUCATIVIDENC_GET_CLASS (self)->allocate_params (self,
      sizeof (IVIDENC2_Params), sizeof (IVIDENC2_DynamicParams),
      sizeof (IVIDENC2_Status), sizeof (IVIDENC2_InArgs),
      sizeof (IVIDENC2_OutArgs));
}

static gboolean
gst_ducati_videnc_allocate_params_default (GstDucatiVidEnc * self,
    gint params_sz, gint dynparams_sz, gint status_sz, gint inargs_sz,
    gint outargs_sz)
{
  self->params = dce_alloc (params_sz);
  memset (self->params, 0, params_sz);
  self->params->size = params_sz;

  self->dynParams = dce_alloc (dynparams_sz);
  memset (self->dynParams, 0, dynparams_sz);
  self->dynParams->size = dynparams_sz;

  self->status = dce_alloc (status_sz);
  memset (self->status, 0, status_sz);
  self->status->size = status_sz;

  self->inBufs = dce_alloc (sizeof (IVIDEO2_BufDesc));
  memset (self->inBufs, 0, sizeof (IVIDEO2_BufDesc));

  self->outBufs = dce_alloc (sizeof (XDM2_BufDesc));
  memset (self->outBufs, 0, sizeof (XDM2_BufDesc));

  self->inArgs = dce_alloc (inargs_sz);
  memset (self->inArgs, 0, inargs_sz);
  self->inArgs->size = inargs_sz;

  self->outArgs = dce_alloc (outargs_sz);
  memset (self->outArgs, 0, outargs_sz);
  self->outArgs->size = outargs_sz;

  GST_INFO_OBJECT (self, "started");

  return TRUE;
}

static gboolean
gst_ducati_videnc_free_params (GstDucatiVidEnc * self)
{
  if (self->params) {
    dce_free (self->params);
    self->params = NULL;
  }

  if (self->dynParams) {
    dce_free (self->dynParams);
    self->dynParams = NULL;
  }

  if (self->inArgs) {
    dce_free (self->inArgs);
    self->inArgs = NULL;
  }

  if (self->outArgs) {
    dce_free (self->outArgs);
    self->outArgs = NULL;
  }

  if (self->status) {
    dce_free (self->status);
    self->status = NULL;
  }

  if (self->inBufs) {
    dce_free (self->inBufs);
    self->inBufs = NULL;
  }

  if (self->outBufs) {
    dce_free (self->outBufs);
    self->outBufs = NULL;
  }

  if (self->codec) {
    VIDENC2_delete (self->codec);
    self->codec = NULL;
  }

  return TRUE;
}

static void
gst_ducati_videnc_close_engine (GstDucatiVidEnc * self)
{
  if (self->engine) {
    Engine_close (self->engine);
    self->engine = NULL;
  }

  if (self->device) {
    dce_deinit (self->device);
    self->device = NULL;
  }
}


static gboolean
ducati_videnc_start (GstVideoEncoder * video_encoder)
{
  GstDucatiVidEnc *self = GST_DUCATIVIDENC (video_encoder);

  self->configure = TRUE;

  if (!gst_ducati_videnc_open_engine (self))
    goto fail;

  if (!gst_ducati_videnc_allocate_params (self))
    goto fail;

  return TRUE;

fail:
  gst_ducati_videnc_free_params (self);
  gst_ducati_videnc_close_engine (self);
  return FALSE;
}

static gboolean
ducati_videnc_stop (GstVideoEncoder * video_encoder)
{
  GstDucatiVidEnc *self = GST_DUCATIVIDENC (video_encoder);

  gst_ducati_videnc_free_params (self);
  gst_ducati_videnc_close_engine (self);

  if (self->input_pool) {
    gst_object_unref (self->input_pool);
    self->input_pool = NULL;
  }

  if (self->output_pool) {
    gst_object_unref (self->output_pool);
    self->output_pool = NULL;
  }

  return TRUE;
}

static GstFlowReturn
ducati_videnc_handle_frame (GstVideoEncoder * video_encoder,
    GstVideoCodecFrame * frame)
{
  int i;
  XDAS_Int32 err;
  GstClockTime t;
  /*GstClockTime ts; */
  GstBuffer *inbuf, *outbuf;
  GstDucatiPriv *input_buffer_priv, *output_buffer_priv;

  GstDucatiVidEnc *self = GST_DUCATIVIDENC (video_encoder);
  GstVideoInfo *info = &self->input_state->info;

  if (G_UNLIKELY (self->configure)) {
    if (!gst_ducati_videnc_configure (self, frame)) {
      GST_DEBUG_OBJECT (self, "configure failed");
      GST_ELEMENT_ERROR (self, STREAM, ENCODE, (NULL), (NULL));

      return GST_FLOW_ERROR;
    }
  }

  /*ts = GST_BUFFER_TIMESTAMP (inbuf); */
have_inbuf:
  input_buffer_priv =
      gst_buffer_add_ducati_priv (frame->input_buffer, self->device, info);
  if (input_buffer_priv == NULL) {
    GstBuffer *newbuf;

    GST_DEBUG_OBJECT (self, "memcpying input");
    if (!gst_buffer_pool_acquire_buffer (self->input_pool, &newbuf, NULL))
      goto alloc_inbuf_failed;

    gst_buffer_copy_into (newbuf, frame->input_buffer, GST_BUFFER_COPY_ALL,
        0, gst_buffer_get_size (frame->input_buffer));

    gst_buffer_unref (frame->input_buffer);
    frame->input_buffer = newbuf;
    goto have_inbuf;
  }

  inbuf = gst_buffer_ref (frame->input_buffer);
  if (gst_buffer_pool_acquire_buffer (self->output_pool, &outbuf, NULL) !=
      GST_FLOW_OK)
    goto alloc_outbuf_failed;

  output_buffer_priv = gst_buffer_add_ducati_priv (outbuf, self->device, info);

  self->inBufs->planeDesc[0].buf =
      (XDAS_Int8 *) omap_bo_handle (input_buffer_priv->bo);
  self->inBufs->planeDesc[0].memType = XDM_MEMTYPE_BO;
  self->inBufs->planeDesc[0].bufSize.tileMem.width = info->width;
  self->inBufs->planeDesc[0].bufSize.tileMem.height = info->height;
  self->inBufs->planeDesc[1].buf = (XDAS_Int8 *) input_buffer_priv->uv_offset;
  self->inBufs->planeDesc[1].memType = XDM_MEMTYPE_BO_OFFSET;
  self->inBufs->planeDesc[1].bufSize.tileMem.width = info->width;
  self->inBufs->planeDesc[1].bufSize.tileMem.height = info->height / 2;

  apply_cropping (self, frame);

  self->inBufs->imagePitch[0] = info->width;
  self->inBufs->imagePitch[1] = info->width;
  self->inBufs->topFieldFirstFlag = TRUE;

  self->outBufs->numBufs = 1;
  self->outBufs->descs[0].buf =
      (XDAS_Int8 *) omap_bo_handle (output_buffer_priv->bo);
  self->outBufs->descs[0].bufSize.bytes = gst_buffer_get_size (outbuf);
  self->outBufs->descs[0].memType = XDM_MEMTYPE_BO;

  /* Givin our ref to the codec */
  self->inArgs->inputID = GPOINTER_TO_INT (inbuf);

  GST_DEBUG_OBJECT (self, "Calling VIDENC2_process");
  t = gst_util_get_timestamp ();
  err = VIDENC2_process (self->codec, self->inBufs, self->outBufs,
      self->inArgs, self->outArgs);
  t = gst_util_get_timestamp () - t;
  GST_DEBUG_OBJECT (self, "VIDENC2_process took %10dns (%d ms)", (gint) t,
      (gint) (t / 1000000));

  if (err) {
    GST_WARNING_OBJECT (self, "process failed: err=%d, extendedError=%08x",
        err, self->outArgs->extendedError);
    gst_ducati_log_extended_error_info (self->outArgs->extendedError,
        self->error_strings);

    err = VIDENC2_control (self->codec,
        XDM_GETSTATUS, (IVIDENC2_DynamicParams *) self->dynParams,
        self->status);

    GST_WARNING_OBJECT (self, "XDM_GETSTATUS: err=%d, extendedError=%08x",
        err, self->status->extendedError);

    return GST_FLOW_ERROR;
  }

  if (self->outArgs->bytesGenerated > 0) {
    if (GST_DUCATIVIDENC_GET_CLASS (self)->is_sync_point (self,
            self->outArgs->encodedFrameType))
      GST_VIDEO_CODEC_FRAME_SET_SYNC_POINT (frame);

    GST_CAT_DEBUG_OBJECT (GST_CAT_PERFORMANCE, self,
            "Encoded frame in %u bytes", self->outArgs->bytesGenerated);
    frame->output_buffer =
        gst_buffer_copy_region (outbuf, GST_BUFFER_COPY_MEMORY, 0,
        self->outArgs->bytesGenerated);

    /* FIXME Check if we actually need to copy the  buffer */

    /* As we can get frames in a different order we sent them (if the codec
       supports B frames and we set it up for generating those), we need to
       work out what input frame corresponds to the frame we just got, to
       keep presentation times correct.
       It seems that the codec will free buffers in the right order for this,
       but I can not find anything saying this in the docs, so:
       - it might be subject to change
       - it might not be true in all setups
       - it might not be true for all codecs
       However, that's the only way I can see to do it. So there's a nice
       assert below that will blow up if the codec does not free exactly one
       input frame when it outputs a frame. That doesn't catch all cases,
       such as when it frees them in the wrong order, but that seems less
       likely to happen.
       The timestamp and duration are given to the base class, which will
       in turn set them onto the encoded buffer. */
    g_assert (self->outArgs->freeBufID[0] && !self->outArgs->freeBufID[1]);
    inbuf = GST_BUFFER (self->outArgs->freeBufID[0]);
#if 0
    frame->pts = GST_BUFFER_TIMESTAMP (inbuf);
    frame->duration = GST_BUFFER_DURATION (inbuf);
    GST_BUFFER_OFFSET_END (frame->output_buffer) = GST_BUFFER_TIMESTAMP (inbuf);
#endif
  }

  gst_buffer_unref (outbuf);

  for (i = 0; self->outArgs->freeBufID[i]; i++) {
    GstBuffer *buf = (GstBuffer *) self->outArgs->freeBufID[i];

    GST_LOG_OBJECT (self, "free buffer: %p", buf);
    gst_buffer_unref (buf);
  }

  return gst_video_encoder_finish_frame (video_encoder, frame);

alloc_inbuf_failed:
  GST_INFO_OBJECT (self, "Could not allocate input buffer");
  return gst_video_encoder_finish_frame (video_encoder, frame);

alloc_outbuf_failed:
  GST_INFO_OBJECT (self, "Could not allocate output buffer");
  gst_buffer_unref (inbuf);
  return gst_video_encoder_finish_frame (video_encoder, frame);
}

static gboolean
ducati_propose_alloc (GstVideoEncoder * encoder, GstQuery * query)
{
  gsize size;
  GstCaps *caps;
  gboolean need_pool;
  GstStructure *config;

  GstBufferPool *pool = NULL;
  GstDucatiVidEnc *self = GST_DUCATIVIDENC (encoder);

  gst_query_parse_allocation (query, &caps, &need_pool);
  if (caps == NULL)
    goto no_caps;

  if (self->input_pool)
    pool = gst_object_ref (self->input_pool);

  if (pool != NULL) {
    GstCaps *pcaps;

    /* we had a pool, check caps */
    GST_DEBUG_OBJECT (self, "check existing pool caps");
    config = gst_buffer_pool_get_config (pool);
    gst_buffer_pool_config_get_params (config, &pcaps, &size, NULL, NULL);
    gst_structure_free (config);

    if (!gst_caps_is_equal (caps, pcaps)) {
      GST_DEBUG_OBJECT (self, "pool has different caps");
      /* different caps, we can't use this pool */
      gst_object_unref (pool);
      pool = NULL;
    }
  }

  if (pool == NULL && need_pool) {
    GstVideoInfo info;

    if (!gst_video_info_from_caps (&info, caps))
      goto invalid_caps;

    GST_DEBUG_OBJECT (self, "create new pool");
    pool = gst_drm_buffer_pool_new (dce_get_fd ());

    /* the normal size of a frame */
    size = info.size;
  }

  if (pool) {
    config = gst_buffer_pool_get_config (pool);
    gst_buffer_pool_config_set_params (config, caps, size, 0, 0);
    if (!gst_buffer_pool_set_config (pool, config))
      goto config_failed;

    /* we need at least 2 buffer because we hold on to the last one */
    gst_query_add_allocation_pool (query, pool, size, 2, 0);
    self->input_pool = pool;
    /*gst_buffer_pool_set_active (pool, TRUE); */
  }

  gst_query_add_allocation_meta (query, GST_DRM_META_API_TYPE, NULL);
  gst_query_add_allocation_meta (query, GST_VIDEO_META_API_TYPE, NULL);
  gst_query_add_allocation_meta (query, GST_DMA_BUF_META_API_TYPE, NULL);
  gst_query_add_allocation_meta (query, GST_VIDEO_CROP_META_API_TYPE, NULL);

  return GST_VIDEO_ENCODER_CLASS (parent_class)->propose_allocation (encoder,
      query);

  /* ERRORS */
no_caps:
  {
    GST_DEBUG_OBJECT (self, "no caps specified");
    return FALSE;
  }
invalid_caps:
  {
    GST_DEBUG_OBJECT (self, "invalid caps specified");
    return FALSE;
  }
config_failed:
  {
    GST_DEBUG_OBJECT (self, "failed setting config");
    gst_object_unref (pool);
    return FALSE;
  }
}

static gboolean
ducati_decide_allocation (GstVideoEncoder * encoder, GstQuery * query)
{
  GstStructure *config;
  GstCaps *outcaps, *caps;

  GstBufferPool *pool = NULL;
  guint nb_pools, min = 0, max = 0, size = 0;

  GstDucatiVidEnc *self = GST_DUCATIVIDENC (encoder);

  if (!GST_VIDEO_ENCODER_CLASS (parent_class)->decide_allocation (encoder,
          query))
    return FALSE;

  gst_query_parse_allocation (query, &outcaps, NULL);
  if ((nb_pools = gst_query_get_n_allocation_pools (query))) {
    guint i;

    for (i = 0; i < nb_pools; i++) {
      gst_query_parse_nth_allocation_pool (query, i, &pool, &size, &min, &max);

      /* Check that it has the proper options */
      if (gst_buffer_pool_has_option (pool, GST_BUFFER_POOL_OPTION_DRM_META) &&
          gst_buffer_pool_has_option (pool, GST_BUFFER_POOL_OPTION_DMABUF_META)
          && gst_buffer_pool_has_option (pool,
              GST_BUFFER_POOL_OPTION_VIDEO_META))
        break;

      gst_object_unref (pool);
      pool = NULL;
    }
  }

  if (self->output_pool) {
    GstStructure *s;

    if (pool == self->output_pool) {
      GST_DEBUG_OBJECT (self, "Proposing the same pool, keeping it");
      gst_object_unref (pool);

      goto done;
    }

    s = gst_buffer_pool_get_config (self->output_pool);
    gst_buffer_pool_config_get_params (s, &caps, NULL, NULL, NULL);
    gst_structure_free (s);
    if (gst_caps_is_equal (caps, outcaps)) {
      GST_DEBUG_OBJECT (self, "Same caps, keeping pool");
      goto done;
    }

    GST_DEBUG_OBJECT (self, "Deactivating previous pool");
    g_object_unref (self->output_pool);
    gst_structure_free (s);
  }

  if (pool) {
    GST_DEBUG_OBJECT (self, "Using proposed pool: %" GST_PTR_FORMAT, pool);
    self->output_pool = pool;
    goto done;
  } else {
    GST_DEBUG_OBJECT (self, "Creating new pool");
    self->output_pool = gst_drm_buffer_pool_new (dce_get_fd ());
  }

done:
  if (size < self->output_state->info.size)
    size = self->output_state->info.size;

  config = gst_buffer_pool_get_config (self->output_pool);
  gst_buffer_pool_config_set_params (config, self->output_state->caps, size,
      min, max);

  /* just set the option, if the pool can support it we will transparently use
   * it through the video info API. We could also see if the pool support this
   * option and only activate it then. */
  gst_buffer_pool_config_add_option (config, GST_BUFFER_POOL_OPTION_VIDEO_META);
  gst_buffer_pool_set_config (self->output_pool, config);

  /* Give our pool as best option */
  if (nb_pools)
    gst_query_set_nth_allocation_pool (query, 0, self->output_pool, size, min,
        max);
  else
    gst_query_add_allocation_pool (query, self->output_pool, size, min, max);

  /* Activate the pool ourself */
  return gst_buffer_pool_set_active (self->output_pool, TRUE);
}

static gboolean
gst_ducati_videnc_is_sync_point_default (GstDucatiVidEnc * enc, int type)
{
  return type == IVIDEO_I_FRAME;
}

static void
gst_ducati_videnc_class_init (GstDucatiVidEncClass * klass)
{
  GObjectClass *gobject_class;
  GstVideoEncoderClass *videoencoder_class;

  gobject_class = G_OBJECT_CLASS (klass);
  videoencoder_class = GST_VIDEO_ENCODER_CLASS (klass);

  gobject_class->set_property = gst_ducati_videnc_set_property;
  gobject_class->get_property = gst_ducati_videnc_get_property;
  gobject_class->finalize = gst_ducati_videnc_finalize;

  videoencoder_class->set_format = GST_DEBUG_FUNCPTR (ducati_videnc_set_format);
  videoencoder_class->start = GST_DEBUG_FUNCPTR (ducati_videnc_start);
  videoencoder_class->stop = GST_DEBUG_FUNCPTR (ducati_videnc_stop);
  videoencoder_class->handle_frame =
      GST_DEBUG_FUNCPTR (ducati_videnc_handle_frame);
  videoencoder_class->propose_allocation =
      GST_DEBUG_FUNCPTR (ducati_propose_alloc);
  videoencoder_class->decide_allocation =
      GST_DEBUG_FUNCPTR (ducati_decide_allocation);

  klass->configure = NULL;
  klass->allocate_params = gst_ducati_videnc_allocate_params_default;
  klass->is_sync_point = gst_ducati_videnc_is_sync_point_default;

  g_object_class_install_property (gobject_class, PROP_BITRATE,
      g_param_spec_int ("bitrate", "Bitrate", "Bitrate in kbit/sec", -1,
          100 * 1024, DEFAULT_BITRATE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_RATE_PRESET,
      g_param_spec_enum ("rate-preset", "H.264 Rate Control",
          "H.264 Rate Control",
          GST_TYPE_DUCATI_VIDENC_RATE_PRESET, DEFAULT_RATE_PRESET,
          G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class, PROP_INTRA_INTERVAL,
      g_param_spec_int ("intra-interval", "Intra-frame interval",
          "Interval between intra frames (keyframes)", 0, INT_MAX,
          DEFAULT_INTRA_INTERVAL, G_PARAM_READWRITE));

  GST_DEBUG_CATEGORY_GET (GST_CAT_PERFORMANCE, "GST_PERFORMANCE");
}

static void
gst_ducati_videnc_init (GstDucatiVidEnc * self)
{
  GST_DEBUG ("gst_ducati_videnc_init");

  gst_ducati_set_generic_error_strings (self->error_strings);


  self->device = NULL;
  self->engine = NULL;
  self->codec = NULL;
  self->params = NULL;
  self->status = NULL;
  self->inBufs = NULL;
  self->outBufs = NULL;
  self->inArgs = NULL;
  self->outArgs = NULL;
  self->input_pool = NULL;
  self->output_pool = NULL;
  self->input_state = NULL;
  self->output_state = NULL;

  self->bitrate = DEFAULT_BITRATE * 1000;
  self->rate_preset = DEFAULT_RATE_PRESET;
  self->intra_interval = DEFAULT_INTRA_INTERVAL;
}
