/*
 * GStreamer
 * Copyright (c) 2010, Texas Instruments Incorporated
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * SECTION:element-ducatijpegdec
 *
 * FIXME:Describe ducatijpegdec here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! ducatijpegdec ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gstducatijpegdec.h"

#define parent_class GST_DUCATIVIDDEC_CLASS (gst_ducati_jpegdec_parent_class)
G_DEFINE_TYPE (GstDucatiJpegDec, gst_ducati_jpegdec, GST_TYPE_DUCATIVIDDEC);

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("image/jpeg, "
        "parsed = (boolean)true, "
        "width = (int)[ 32, 2048 ], "
        "height = (int)[ 32, 2048 ], " "framerate = (fraction)[ 0, max ];")
    );

/* GstDucatiVidDec vmethod implementations */

static void
gst_ducati_jpegdec_update_buffer_size (GstDucatiVidDec * self)
{
  gint w = self->width;
  gint h = self->height;

  /* calculate output buffer parameters: */
  self->padded_width = w;
  self->padded_height = h;
  self->min_buffers = 1;
}

static gboolean
gst_ducati_jpegdec_allocate_params (GstDucatiVidDec * self, gint params_sz,
    gint dynparams_sz, gint status_sz, gint inargs_sz, gint outargs_sz)
{
  IJPEGVDEC_DynamicParams *dynParams;

  gboolean ret = parent_class->allocate_params (self,
      sizeof (IJPEGVDEC_Params), sizeof (IJPEGVDEC_DynamicParams),
      sizeof (IJPEGVDEC_Status), sizeof (IJPEGVDEC_InArgs),
      sizeof (IJPEGVDEC_OutArgs));

  if (!ret)
    return ret;

  /* We're doing ENTIREFRAME decoding so in theory 0 should be a valid value
   * for this. The codec seems to check that it's non-zero though...
   */
  self->params->numOutputDataUnits = 1;

  dynParams = (IJPEGVDEC_DynamicParams *) self->dynParams;
  dynParams->decodeThumbnail = 0;
  dynParams->thumbnailMode = 3;
  dynParams->downsamplingFactor = 1;
  dynParams->streamingCompliant = 0;

  return ret;
}

/* GObject vmethod implementations */
static void
gst_ducati_jpegdec_class_init (GstDucatiJpegDecClass * klass)
{
  GstDucatiVidDecClass *bclass = GST_DUCATIVIDDEC_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  bclass->codec_name = "ivahd_jpegvdec";
  bclass->update_buffer_size =
      GST_DEBUG_FUNCPTR (gst_ducati_jpegdec_update_buffer_size);
  bclass->allocate_params =
      GST_DEBUG_FUNCPTR (gst_ducati_jpegdec_allocate_params);

  gst_element_class_set_static_metadata (element_class,
      "DucatiJpegDec",
      "Codec/Decoder/Video",
      "Decodes video in MPEG-2 format with ducati",
      "Alessandro Decina <alessandro.decina@collabora.co.uk>");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));
}

static void
gst_ducati_jpegdec_init (GstDucatiJpegDec * self)
{
  GstDucatiVidDec *vdec = GST_DUCATIVIDDEC (self);
  vdec->pageMemType = XDM_MEMTYPE_RAW;
}
