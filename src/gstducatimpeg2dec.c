/*
 * GStreamer
 * Copyright (c) 2010, Texas Instruments Incorporated
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * SECTION:element-ducatimpeg2dec
 *
 * FIXME:Describe ducatimpeg2dec here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! ducatimpeg2dec ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gstducatimpeg2dec.h"

#define gst_ducati_mpeg2dec_parent_class parent_class
G_DEFINE_TYPE (GstDucatiMpeg2Dec, gst_ducati_mpeg2dec, GST_TYPE_DUCATIVIDDEC);

/* *INDENT-OFF* */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (
      "video/mpeg, "
        "mpegversion = (int)[ 1, 2 ], "     // XXX check on MPEG-1..
        "systemstream = (boolean)false, "
        "parsed = (boolean)true, "
        "width = (int)[ 64, 2048 ], "
        "height = (int)[ 64, 2048 ], "
        "framerate = (fraction)[ 0, max ];")
    );
/* *INDENT-ON* */

/* GstDucatiVidDec vmethod implementations */

static void
gst_ducati_mpeg2dec_update_buffer_size (GstDucatiVidDec * self)
{
  gint w = self->width;
  gint h = self->height;

  /* calculate output buffer parameters: */
  self->padded_width = w;
  self->padded_height = h;
  self->min_buffers = 8;
}

static gboolean
gst_ducati_mpeg2dec_allocate_params (GstDucatiVidDec * self, gint params_sz,
    gint dynparams_sz, gint status_sz, gint inargs_sz, gint outargs_sz)
{
  gboolean ret = GST_DUCATIVIDDEC_CLASS (parent_class)->allocate_params (self,
      sizeof (IVIDDEC3_Params), sizeof (IVIDDEC3_DynamicParams),
      sizeof (IVIDDEC3_Status), sizeof (IVIDDEC3_InArgs),
      sizeof (IVIDDEC3_OutArgs));

  return ret;
}

static GstFlowReturn
videodecoder_push_input (GstDucatiVidDec * vdec, GstVideoCodecFrame * frame)
{
  GstMapInfo cdata_map, ibuf_map;

  gboolean has_codec_data = FALSE;
  GstDucatiMpeg2Dec *self = GST_DUCATIMPEG2DEC (vdec);

  gst_buffer_map (frame->input_buffer, &ibuf_map, GST_MAP_READ);
  if (vdec->input_state->codec_data) {
    gst_buffer_map (vdec->input_state->codec_data, &cdata_map, GST_MAP_READ);
    has_codec_data = TRUE;
  }

  /* skip codec_data, which is same as first buffer from mpegvideoparse (and
   * appears to be periodically resent) and instead prepend to next frame..
   */
  if (has_codec_data && (ibuf_map.size == cdata_map.size) &&
      !memcmp (ibuf_map.data, cdata_map.data, ibuf_map.size)) {
    GST_DEBUG_OBJECT (self, "skipping codec_data buffer");
    self->prepend_codec_data = TRUE;
  } else {
    if (self->prepend_codec_data) {
      GST_DEBUG_OBJECT (self, "prepending codec_data buffer");
      push_input (vdec, cdata_map.data, cdata_map.size);
      self->prepend_codec_data = FALSE;
    }
    push_input (vdec, ibuf_map.data, ibuf_map.size);
  }

  if (has_codec_data)
    gst_buffer_unmap (vdec->input_state->codec_data, &cdata_map);

  return GST_FLOW_OK;
}

/* FIXME GstVideoDecoder: Make it make sense again
static GstFlowReturn
gst_ducati_mpeg2dec_push_frame (GstDucatiVidDec * self, GstVideoCodecFrame * frame)
{
  GST_BUFFER_OFFSET_END (buf) = GST_BUFFER_TIMESTAMP (buf);
  return parent_class->push_frame (self, );
} */

/* GObject vmethod implementations */

static void
gst_ducati_mpeg2dec_class_init (GstDucatiMpeg2DecClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstDucatiVidDecClass *bclass = GST_DUCATIVIDDEC_CLASS (klass);

  gst_element_class_set_static_metadata (element_class,
      "DucatiMpeg2Dec",
      "Codec/Decoder/Video",
      "Decodes video in MPEG-2 format with ducati", "Rob Clark <rob@ti.com>");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  bclass->codec_name = "ivahd_mpeg2vdec";
  bclass->update_buffer_size =
      GST_DEBUG_FUNCPTR (gst_ducati_mpeg2dec_update_buffer_size);
  bclass->allocate_params =
      GST_DEBUG_FUNCPTR (gst_ducati_mpeg2dec_allocate_params);
  bclass->push_input = GST_DEBUG_FUNCPTR (videodecoder_push_input);
}

static void
gst_ducati_mpeg2dec_init (GstDucatiMpeg2Dec * self)
{
  GstDucatiVidDec *vdec = GST_DUCATIVIDDEC (self);
  vdec->pageMemType = XDM_MEMTYPE_RAW;
}
