/*
 * GStreamer
 *
 * Copyright (C) 2012 Texas Instruments
 * Copyright (C) 2012 Collabora Ltd
 *
 * Authors:
 *  Alessandro Decina <alessandro.decina@collabora.co.uk>
 *  Rob Clark <rob.clark@linaro.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdint.h>
#include <gst/gst.h>

#include <omap_drm.h>
#include <omap_drmif.h>
#include <xf86drmMode.h>

#include "gstducatipriv.h"
#include <sys/dmabuf/gstdmabufmeta.h>

#define PRIV_QUARK_STR "GstDucatiPriv"


GType _gst_ducati_priv_type;
GST_DEFINE_MINI_OBJECT_TYPE (GstDucatiPriv, gst_ducati_priv);

static inline GstDucatiPriv *
gst_buffer_get_ducati_priv (GstBuffer * buf)
{
  return GST_DUCATI_PRIV (gst_mini_object_get_qdata (GST_MINI_OBJECT (buf),
          g_quark_from_string (PRIV_QUARK_STR)));
}

static void
gst_ducati_priv_finalize (GstDucatiPriv * priv)
{
  omap_bo_del (priv->bo);
}

static GstDucatiPriv *
gst_ducati_priv_new (void)
{
  GstDucatiPriv *priv = g_slice_new0 (GstDucatiPriv);

  gst_mini_object_init (GST_MINI_OBJECT_CAST (priv),
      0, _gst_ducati_priv_type, NULL, NULL,
      (GstMiniObjectFreeFunction) gst_ducati_priv_finalize);

  return priv;
}

GstDucatiPriv *
gst_buffer_add_ducati_priv (GstBuffer * buffer,
    struct omap_device * device, GstVideoInfo * info)
{
  GstDmaBufMeta *dmabufmeta = NULL;
  GstDucatiPriv *priv = gst_buffer_get_ducati_priv (buffer);

  if (!priv) {
    struct omap_bo *bo;

    dmabufmeta = gst_buffer_get_dma_buf_meta (buffer);
    if (!dmabufmeta)
      goto not_dmabuf;

    if (!(bo = omap_bo_from_dmabuf (device, dmabufmeta->fd)))
      goto create_bo_failed;

    priv = gst_ducati_priv_new ();
    gst_mini_object_set_qdata (GST_MINI_OBJECT (buffer),
        g_quark_from_string (PRIV_QUARK_STR), priv,
        (GDestroyNotify) gst_mini_object_unref);
    priv->bo = bo;
    priv->uv_offset = info->offset[1];
    priv->size = info->size;
  }

  return priv;

not_dmabuf:
  GST_WARNING ("Can't do anything without dmabuf meta");
  return NULL;

create_bo_failed:
  GST_WARNING ("Can't create omap bo");
  return NULL;
}
